package com.example.criminalintent;

import java.util.Date;
import java.util.UUID;

public class Crime {
    private UUID mId;
    private String mTitle;

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public Date getmDate() {
        return mDate;
    }

    public void setmDate(Date mDate) {
        this.mDate = mDate;
    }

    public boolean ismSovled() {
        return mSovled;
    }

    public void setmSovled(boolean mSovled) {
        this.mSovled = mSovled;
    }

    private Date mDate;
    private  boolean mSovled;

    public UUID getmId() {
        return mId;
    }

    public void setmId(UUID mId) {
        this.mId = mId;
    }

    public  Crime(){
        mId=UUID.randomUUID();
        mDate=new Date();
    }
}
